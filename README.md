# HelloFX

A simple Hello World application with Java 11+, JavaFX 15+ and GraalVM.

## Documentation

Read about this sample [here](https://docs.gluonhq.com/#_hellofx)

---
[https://foojay.io/today/creating-mobile-apps-with-javafx-part-2/#comment-2650](https://foojay.io/today/creating-mobile-apps-with-javafx-part-2/#comment-2650)

Hi Siegfried,
Thanks for the comment and questions. Your questions sent me checking with those more involved in the Android-specific Gluon Substrate details than I am. Here is a response from @JPeredaDnr, engineer extraordinaire at Gluon:

Android runs its own 1.7 JVM, and we [Gluon substrate] add ours (via GraalVM, 17), and both need to communicate.
If you check your project, under target/gluonfx/aarch64-android/gvm/android_project, there is a full Android project (you can open it with Android Studio). This is created by Substrate.
It has the main activity set as com.gluonhq.helloandroid.MainActivity, which is just the bootstrap activity to launch the “real” app, that runs under libsubstrate.so, by starting the graal thread: https://github.com/gluonhq/substrate/blob/master/src/main/resources/native/android/android_project/app/src/main/java/com/gluonhq/helloandroid/MainActivity.java#L130
In any case, from the Android point of view, this is the entry point, and therefore it has to be present in the AndroidManifest.

In theory you could rename this main activity to your liking, but you will most likely get tons of issues…

For the second question on running an android app in full immersive mode, you can’t specify immersive mode in the AndroidManifest file. So you have to use Attach [the Gluon way to access system or device-specific features]. Attach doesn’t have that service yet, but it should be fairly easy to do. You need to modify some of the Intents with those flags.

You can create a service with AttachExtended, and give it a try, or if you need to modify the MainActivity you can manually build the apk:

```
mvn -Pandroid gluonfx:package
cd target/gluonfx/aarch64-android/gvm/android_project/
export ANDROID_SDK_ROOT=~/.gluon/substrate/Android
./gradlew app:assembleDebug
```

and then resume

```
mvn -Pandroid gluonfx:install…
```

---

## Quick Instructions

We use [GluonFX plugin](https://docs.gluonhq.com/) to build a native image for platforms including desktop, android and iOS.
Please follow the prerequisites as stated [here](https://docs.gluonhq.com/#_requirements).

### Desktop

Run the application on JVM/HotSpot:

    mvn gluonfx:run

Run the application and explore all scenarios to generate config files for the native image with:

    mvn gluonfx:runagent

Build a native image using:

    mvn gluonfx:build

Run the native image app:

    mvn gluonfx:nativerun

### Android

Build a native image for Android using:

    mvn gluonfx:build -Pandroid

Package the native image as an 'apk' file:

    mvn gluonfx:package -Pandroid
    
Build & Package

	mvn clean gluonfx:build gluonfx:package -Pandroid

Install it on a connected android device:

    mvn gluonfx:install -Pandroid

Run the installed app on a connected android device:

    mvn gluonfx:nativerun -Pandroid

### iOS

Build a native image for iOS using:

    mvn gluonfx:build -Pios

Install and run the native image on a connected iOS device:

    mvn gluonfx:nativerun -Pios

Create an IPA file (for submission to TestFlight or App Store):

    mvn gluonfx:package -Pios

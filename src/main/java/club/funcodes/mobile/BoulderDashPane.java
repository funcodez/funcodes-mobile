// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany and licensed
// under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package club.funcodes.mobile;

import org.refcodes.boulderdash.BoulderDashAutomaton;
import org.refcodes.boulderdash.BoulderDashCaveMap;
import org.refcodes.boulderdash.BoulderDashViewer;
import org.refcodes.boulderdash.FxBoulderDashSpriteFactory;
import org.refcodes.boulderdash.Rockford;
import org.refcodes.checkerboard.alt.javafx.FxChessboardFactory;
import org.refcodes.component.Startable;
import org.refcodes.data.Side;
import org.refcodes.graphical.MoveMode;
import org.refcodes.graphical.ScaleMode;
import org.refcodes.graphical.Viewport;

import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

/**
 * The {@link BoulderDashPane} represents a single boulder dash's maze
 * {@link Pane}.
 */
public class BoulderDashPane extends AnchorPane implements Viewport, Startable {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final int MENU_WIDTH = 7;

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private int _boardLoopTimeMillis;
	private BoulderDashAutomaton _boulderDashAutomaton;
	private BoulderDashViewer _boulderDashViewer;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new {@link BoulderDashPane} with the given
	 * {@link BoulderDashCaveMap} and the according properties..
	 *
	 * @param aCaveMap The {@link BoulderDashCaveMap} to use.
	 * @param aScreenWidth The desired screen width to use.
	 * @param aScreenHeight The desired screen height to use.
	 * @param aViewportWidth The required viewport width.
	 * @param aViewportHeight The required viewport height.
	 * @param aViewportFillColor The viewport fill {@link Color} to use,
	 * @param aOddFieldColor The odd field's {@link Color} to use.
	 * @param aEvenFieldColor The even field's {@link Color} to use.
	 * @param aMenuAlignment The {@link Side} to use.
	 * @param aMenuFillColor The menu fill {@link Color} to use.
	 * @param aMenuStrokeColor The menu stroke {@link Color} to use.
	 * @param aMenuOpacity The menu's opacity to use (its on top of the board).
	 * @param aBoardLoopTimeMillis The board's loop time in milliseconds, each
	 *        loop is a cycle (processing all fields of the board) of the
	 *        cellular automation.
	 */
	public BoulderDashPane( BoulderDashCaveMap aCaveMap, double aScreenWidth, double aScreenHeight, int aViewportWidth, int aViewportHeight, Color aViewportFillColor, Color aOddFieldColor, Color aEvenFieldColor, Side aMenuAlignment, Color aMenuFillColor, Color aMenuStrokeColor, double aMenuOpacity, int aBoardLoopTimeMillis ) {
		_boardLoopTimeMillis = aBoardLoopTimeMillis;
		int theFieldWidthPx = (int) aScreenWidth / aViewportWidth;
		int theFieldHeightPx = (int) aScreenHeight / aViewportHeight;
		theFieldWidthPx = theFieldWidthPx > theFieldHeightPx ? theFieldHeightPx : theFieldWidthPx;
		theFieldHeightPx = theFieldHeightPx > theFieldWidthPx ? theFieldWidthPx : theFieldHeightPx;
		int theViewportWidth = (int) (aScreenWidth / theFieldWidthPx) - MENU_WIDTH;
		int theViewportHeight = (int) aScreenHeight / theFieldHeightPx;
		int theTotalHeightPx = theViewportHeight * theFieldHeightPx;
		int theViewportGapfPx = (int) (aScreenWidth - (theViewportWidth + MENU_WIDTH) * theFieldWidthPx);
		int theMenuWidthPx = theFieldWidthPx * MENU_WIDTH + theViewportGapfPx;
		int theMenuX = aMenuAlignment == Side.LEFT ? 0 : (int) aScreenWidth - theMenuWidthPx;
		int theMenuY = 0;
		int theMenuHeightPx = theTotalHeightPx - theMenuY * 2;
		int theArcWidthPx = theFieldWidthPx;
		int theArcHeightPx = theFieldHeightPx;
		int theViewPortXPx = aMenuAlignment == Side.LEFT ? theMenuWidthPx : 0;
		int theViewPortYPx = 0;
		_boulderDashAutomaton = new BoulderDashAutomaton( aCaveMap );
		FxChessboardFactory theBackgroundFactory = new FxChessboardFactory().withOddFieldColor( aOddFieldColor ).withEvenFieldColor( aEvenFieldColor );
		FxBoulderDashSpriteFactory theSpriteFactory = new FxBoulderDashSpriteFactory().withScaleFactor( 1 );
		_boulderDashViewer = new BoulderDashViewer( _boulderDashAutomaton );
		_boulderDashViewer.withSpriteFactory( theSpriteFactory ).withBackgroundFactory( theBackgroundFactory ).withMoveMode( MoveMode.SMOOTH ).withMovePlayerDurationMillis( 5 );
		_boulderDashViewer.withDragOpacity( 0.9 ).withScaleMode( ScaleMode.NONE ).withMoveMode( MoveMode.SMOOTH );
		_boulderDashViewer.withFieldDimension( theFieldWidthPx, theFieldHeightPx ).withViewportDimension( theViewportWidth, theViewportHeight );
		_boulderDashViewer.setLayoutX( theViewPortXPx );
		_boulderDashViewer.setLayoutY( theViewPortYPx );
		Rectangle theMenuPane = new Rectangle( theMenuX, theMenuY, theMenuWidthPx, theMenuHeightPx );
		theMenuPane.setArcWidth( theArcWidthPx );
		theMenuPane.setArcHeight( theArcHeightPx );
		theMenuPane.setFill( aMenuFillColor );
		theMenuPane.setStroke( aMenuStrokeColor );
		theMenuPane.setOpacity( aMenuOpacity );
		getChildren().addAll( _boulderDashViewer, theMenuPane );
		setBackground( new Background( new BackgroundFill( aViewportFillColor, null, null ) ) );
		// _boulderDashViewer.setChangePlayerStateMillis( 0 );
	}

	// /////////////////////////////////////////////////////////////////////////
	// INJECTION:
	// /////////////////////////////////////////////////////////////////////////

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void start() {
		_boulderDashViewer.initialize();
		_boulderDashAutomaton.start( _boardLoopTimeMillis );
	}

	/**
	 * Retrieves {@link Rockford} from the internally used
	 * {@link BoulderDashAutomaton}.
	 * 
	 * @return The {@link Rockford} instance or null if the
	 *         {@link BoulderDashAutomaton} does not contain any
	 *         {@link Rockford} instance.
	 */
	public Rockford getRockford() {
		return _boulderDashAutomaton.firstPlayer( Rockford.class );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getViewportHeight() {
		return _boulderDashViewer.getViewportHeight();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getViewportWidth() {
		return _boulderDashViewer.getViewportWidth();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void centerViewortOffset( int aPositionX, int aPositionY, int aViewportWidth, int aViewportHeight ) {
		_boulderDashViewer.centerViewortOffset( aPositionX, aPositionY, aViewportWidth, aViewportHeight );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void centerViewortOffset( int aPositionX, int aPositionY ) {
		_boulderDashViewer.centerViewortOffset( aPositionX, aPositionY );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

}

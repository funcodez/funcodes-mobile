// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany and licensed
// under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package club.funcodes.mobile;

import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Screen;

/**
 * The {@link SysinfoPane} displays some information such as detected
 * resolutions (useful for debugging when not having debug means on the target
 * device).
 */
public class SysinfoPane extends Pane {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new {@link SysinfoPane}.
	 * 
	 * @param aScreenWidth The desired screen width to use.
	 * @param aScreenHeight The desired screen height to use.
	 * @param isDebug When true display more debug data.
	 * @param aBackAction The action to perform when hitting the "back" button.
	 */
	public SysinfoPane( double aScreenWidth, double aScreenHeight, boolean isDebug, Runnable aBackAction ) {
		double theScreenCenterX = aScreenWidth / 2;
		double theScreenCenterY = aScreenHeight / 2;
		double theCircleStroke = aScreenHeight / 256 < 0 ? 1 : aScreenHeight / 256;
		double theCircleRadius = aScreenHeight / 16;
		Circle theTopLeftCircle = new Circle( theCircleRadius );
		theTopLeftCircle.setStrokeWidth( theCircleStroke );
		theTopLeftCircle.setStroke( Color.YELLOW );
		theTopLeftCircle.setCenterX( 0 );
		theTopLeftCircle.setCenterY( 0 );
		Circle theTopRightCircle = new Circle( theCircleRadius );
		theTopRightCircle.setStrokeWidth( theCircleStroke );
		theTopRightCircle.setStroke( Color.YELLOW );
		theTopRightCircle.setCenterX( aScreenWidth );
		theTopRightCircle.setCenterY( 0 );
		Circle theBottomLeftCircle = new Circle( theCircleRadius );
		theBottomLeftCircle.setStrokeWidth( theCircleStroke );
		theBottomLeftCircle.setStroke( Color.YELLOW );
		theBottomLeftCircle.setCenterX( 0 );
		theBottomLeftCircle.setCenterY( aScreenHeight );
		Circle theBottomRightCircle = new Circle( theCircleRadius );
		theBottomRightCircle.setStrokeWidth( theCircleStroke );
		theBottomRightCircle.setStroke( Color.YELLOW );
		theBottomRightCircle.setCenterX( aScreenWidth );
		theBottomRightCircle.setCenterY( aScreenHeight );
		Circle theCenterCircle = new Circle( theCircleRadius );
		theCenterCircle.setStrokeWidth( theCircleStroke );
		theCenterCircle.setStroke( Color.WHITE );
		theCenterCircle.setCenterX( theScreenCenterX );
		theCenterCircle.setCenterY( theScreenCenterY );
		Circle theNextCircle = new Circle( theCircleRadius );
		theNextCircle.setStrokeWidth( theCircleStroke );
		theNextCircle.setStroke( Color.RED );
		theNextCircle.setCenterX( theScreenCenterX + theScreenCenterX / 2 );
		theNextCircle.setCenterY( theScreenCenterY + theScreenCenterY / 2 );
		theNextCircle.setOnTouchPressed( ( evt ) -> { aBackAction.run(); evt.consume(); } );
		theNextCircle.setOnMouseClicked( ( evt ) -> { aBackAction.run(); evt.consume(); } );
		Text theText = toSysinfo( aScreenWidth, aScreenHeight, isDebug );
		getChildren().addAll( theTopRightCircle, theTopLeftCircle, theBottomLeftCircle, theBottomRightCircle, theCenterCircle, theNextCircle, theText );
		setPrefWidth( aScreenWidth );
		setPrefHeight( aScreenHeight );
	}

	// /////////////////////////////////////////////////////////////////////////
	// INJECTION:
	// /////////////////////////////////////////////////////////////////////////

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	private Text toSysinfo( double aScreenWidth, double aScreenHeight, boolean isDebug ) {
		double theScreenWidth = Screen.getPrimary().getBounds().getWidth();
		double theScreenHeight = Screen.getPrimary().getBounds().getHeight();
		double thePosX = theScreenHeight / 64;
		double thePosY = thePosX;
		StringBuilder theTextBuilder = new StringBuilder();
		theTextBuilder.append( "aUsedWidth=" + aScreenWidth );
		theTextBuilder.append( ", UsedHeight=" + theScreenHeight );
		theTextBuilder.append( "\naScreenWidth=" + theScreenWidth );
		theTextBuilder.append( ", aScreenHeight=" + theScreenHeight );
		theTextBuilder.append( "\nBoundsWidth=" + Screen.getPrimary().getBounds().getWidth() );
		theTextBuilder.append( ", BoundsHeight=" + Screen.getPrimary().getBounds().getHeight() );
		theTextBuilder.append( "\nBoundsMinX=" + Screen.getPrimary().getBounds().getMinX() );
		theTextBuilder.append( ", BoundsMinY=" + Screen.getPrimary().getBounds().getMinY() );
		theTextBuilder.append( "\nBoundsMaxX=" + Screen.getPrimary().getBounds().getMaxX() );
		theTextBuilder.append( ", BoundsMaxY=" + Screen.getPrimary().getBounds().getMaxY() );
		if ( isDebug ) {
			theTextBuilder.append( "\nVisualBoundsMinX=" + Screen.getPrimary().getVisualBounds().getMinX() );
			theTextBuilder.append( ", VisualBoundsMinY=" + Screen.getPrimary().getVisualBounds().getMinY() );
			theTextBuilder.append( "\nVisualBoundsMaxX=" + Screen.getPrimary().getVisualBounds().getMaxX() );
			theTextBuilder.append( ", VisualBoundsMaxY=" + Screen.getPrimary().getVisualBounds().getMaxY() );
			theTextBuilder.append( "\nVisualBoundsWidth = " + Screen.getPrimary().getVisualBounds().getWidth() );
			theTextBuilder.append( ", VisualBoundsHeight= " + Screen.getPrimary().getVisualBounds().getHeight() );
		}
		theTextBuilder.append( "\nOutputScaleX=" + Screen.getPrimary().getOutputScaleX() );
		theTextBuilder.append( ", OutputScaleY=" + Screen.getPrimary().getOutputScaleY() );
		Text theTextField = new Text( thePosX, thePosY, theTextBuilder.toString() );
		theTextField.setFill( Color.WHITE );
		theTextField.setFont( Font.font( "MONOSPACED" ) );
		return theTextField;
	}

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

}

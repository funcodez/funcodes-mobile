// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany and licensed
// under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////
package club.funcodes.mobile;

import static org.refcodes.cli.CliSugar.*;

import java.util.List;
import java.util.Random;

import org.refcodes.boulderdash.BoulderDashCaveMap;
import org.refcodes.cli.ArgsParser;
import org.refcodes.cli.ArgsParserImpl;
import org.refcodes.cli.ArgsSyntax;
import org.refcodes.cli.EnumOption;
import org.refcodes.cli.Example;
import org.refcodes.cli.Flag;
import org.refcodes.cli.IntOption;
import org.refcodes.data.AsciiColorPalette;
import org.refcodes.data.Side;
import org.refcodes.graphical.ext.javafx.FxGraphicalUtility;
import org.refcodes.textual.Font;
import org.refcodes.textual.FontFamily;
import org.refcodes.textual.FontStyle;
import org.refcodes.textual.VerboseTextBuilder;

import javafx.application.Application;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.stage.Screen;
import javafx.stage.Stage;

public class MainFx extends Application {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final String NAME = "softcell";
	private static final String TITLE = "S-O-F-T-C-E-L-L";
	private static final char[] BANNER_PALETTE = AsciiColorPalette.MAX_LEVEL_GRAY.getPalette();
	private static final Font BANNER_FONT = new Font( FontFamily.DIALOG, FontStyle.BOLD );
	private static final String COPYRIGHT = "Copyright (c) by CLUB.FUNCODES (see [https://www.funcodes.club])";
	private static final String LICENSE_NOTE = "Licensed under GNU General Public License, v3.0 and Apache License, v2.0";
	private static final String DESCRIPTION = "Demo application for playing around with mobile (android) development.";
	private static final String FUNCODES_ICON_PNG = "funcodes-icon.png";
	private static final String WIDTH_PROPERTY = "width";
	private static final String HEIGHT_PROPERTY = "height";
	private static final String MENU_ALIGNMENT_PROPERTY = "menuAlignment";
	private static final int VIEWPORT_WIDTH = 25; // 30;
	private static final int VIEWPORT_HEIGHT = 15; // 18;
	private static final int BORAD_LOOP_TIME_MILLIS = 150;
	private static final double MENU_OPACITY = 0.75;
	private static final Color MENU_FILL_COLOR = Color.BLACK;
	private static final Color MENU_STROKE_COLOR = Color.gray( 1, 0.5 );
	private static final Color VIEWPORT_FILL_COLOR = Color.rgb( 0x3f, 0x3f, 0x3f );
	private static final Color ODD_FIELD_COLOR = Color.rgb( 0x00, 0x00, 0x00 );
	private static final Color EVEN_FIELD_COLOR = Color.rgb( 0x0f, 0x0f, 0x0f );

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * The main method.
	 *
	 * @param args The arguments.
	 */
	public static void main( String[] args ) {
		launch( args );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void start( Stage aPrimaryStage ) throws Exception {
		IntOption theWidthArg = intOption( "-w", "--width", WIDTH_PROPERTY, "The width (in pixels) to use for the screen." );
		IntOption theHeightArg = intOption( "-h", "--height", HEIGHT_PROPERTY, "The height (in pixels) to use for the screen." );
		EnumOption<Side> theMenuAlignmentArg = enumOption( "-m", "--menu-alignment", Side.class, MENU_ALIGNMENT_PROPERTY, "The position of the menu: " + VerboseTextBuilder.asString( Side.values() ) );
		Flag theSysInfoFlag = sysInfoFlag();
		Flag theVerboseFlag = verboseFlag();
		Flag theHelpFlag = helpFlag();
		Flag theDebugFlag = debugFlag();

		// @formatter:off
		ArgsSyntax theArgsSyntax = cases( 
			optional( and( theWidthArg, theHeightArg ), theMenuAlignmentArg, theVerboseFlag, theDebugFlag ),
			xor( theHelpFlag , and( theSysInfoFlag, any ( theVerboseFlag ) ) )
		);
		Example[] theExamples = examples(
			example( "Run (with a custom config in reach), be quiet", theDebugFlag ),
			example( "To show the help text", theHelpFlag ),
			example( "To print the system info", theSysInfoFlag )
		);
		// @formatter:on

		List<String> theRaw = getParameters().getRaw();
		String[] theArgs = theRaw != null ? theRaw.toArray( new String[theRaw.size()] ) : new String[] {};
		ArgsParser theArgsParser = new ArgsParserImpl( theArgsSyntax ).withExamples( theExamples ).withName( NAME ).withTitle( TITLE ).withDescription( DESCRIPTION ).withLicenseNote( LICENSE_NOTE ).withCopyrightNote( COPYRIGHT ).withBannerFont( BANNER_FONT ).withBannerFontPalette( BANNER_PALETTE );
		theArgsParser.evalArgs( theArgs );

		Scene theErrorScene = null;

		try {
			aPrimaryStage.getIcons().add( FxGraphicalUtility.toImage( Main.class.getResourceAsStream( "/" + FUNCODES_ICON_PNG ) ) );
			aPrimaryStage.setTitle( TITLE );
			boolean isDebug = theDebugFlag.isEnabled();
			try {
				Rectangle2D theBounds = Screen.getPrimary().getBounds();
				Integer theScreenWidth = theWidthArg.getValue() != null ? theWidthArg.getValue() : (int) theBounds.getWidth();
				Integer theScreenHeight = theHeightArg.getValue() != null ? theHeightArg.getValue() : (int) theBounds.getHeight();
				Side theMenuAlignment = theMenuAlignmentArg.getValueOr( Side.RIGHT );
				int theIndex = new Random( System.currentTimeMillis() ).nextInt( BoulderDashCaveMap.values().length );
				BoulderDashCaveMap theCaveMap = BoulderDashCaveMap.values()[theIndex];
				BoulderDashScene theBoulderDashScene = new BoulderDashScene( theCaveMap, theScreenWidth, theScreenHeight, VIEWPORT_WIDTH, VIEWPORT_HEIGHT, VIEWPORT_FILL_COLOR, ODD_FIELD_COLOR, EVEN_FIELD_COLOR, theMenuAlignment, MENU_FILL_COLOR, MENU_STROKE_COLOR, MENU_OPACITY, BORAD_LOOP_TIME_MILLIS );
				SysinfoScene theSysInfoScene = new SysinfoScene( theScreenWidth, theScreenHeight, isDebug, () -> { aPrimaryStage.setScene( theBoulderDashScene ); theBoulderDashScene.start(); } );
				aPrimaryStage.setScene( theBoulderDashScene );
				theBoulderDashScene.start();
				aPrimaryStage.setResizable( false );
				aPrimaryStage.show();
				theErrorScene = theSysInfoScene;
				// theBoulderDashScene.setOnTouchPressed( ( evt ) -> { aPrimaryStage.setScene( theSysInfoScene ); evt.consume(); } );
				// theBoulderDashScene.setOnMouseClicked( ( evt ) -> { aPrimaryStage.setScene( theSysInfoScene ); evt.consume(); } );
				//	Timer theTimer = new Timer( true );
				//	TimerTask theTimerTask = new TimerTask() {
				//		@Override
				//		public void run() {
				//			Platform.runLater( () -> aPrimaryStage.setScene( theSysInfoScene ) );
				//		}
				//	};
				//	theTimer.schedule( theTimerTask, 10000 );
			}
			catch ( Error e ) {
				aPrimaryStage.setScene( theErrorScene );
			}
		}
		catch ( Exception e ) {
			System.err.println( e.getMessage() );
			e.printStackTrace();
			System.exit( e.hashCode() % 0xFF );
		}
	}
}
// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany and licensed
// under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package club.funcodes.mobile;

import org.refcodes.boulderdash.BoulderDashCaveMap;
import org.refcodes.boulderdash.Rockford;
import org.refcodes.component.Startable;
import org.refcodes.data.Side;

import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.scene.paint.Color;

/**
 * The {@link BoulderDashScene} represents a single boulder dash's maze
 * {@link Scene}.
 */
public class BoulderDashScene extends Scene implements Startable {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private BoulderDashPane _boulderDashPane;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new {@link BoulderDashScene} (using a
	 * {@link BoulderDashPane}) with the given {@link BoulderDashCaveMap} and
	 * the according properties, additional hooking onto keyboard events.
	 *
	 * @param aCaveMap The {@link BoulderDashCaveMap} to use.
	 * @param aScreenWidth The desired screen width to use.
	 * @param aScreenHeight The desired screen height to use.
	 * @param aViewportWidth The required viewport width.
	 * @param aViewportHeight The required viewport height.
	 * @param aViewportFillColor The viewport fill {@link Color} to use,
	 * @param aOddFieldColor The odd field's {@link Color} to use.
	 * @param aEvenFieldColor The even field's {@link Color} to use.
	 * @param aMenuAlignment The {@link Side} to use.
	 * @param aMenuFillColor The menu fill {@link Color} to use.
	 * @param aMenuStrokeColor The menu stroke {@link Color} to use.
	 * @param aMenuOpacity The menu's opacity to use (its on top of the board).
	 * @param aBoardLoopTimeMillis The board's loop time in milliseconds, each
	 *        loop is a cycle (processing all fields of the board) of the
	 *        cellular automation.
	 */
	public BoulderDashScene( BoulderDashCaveMap aCaveMap, double aScreenWidth, double aScreenHeight, int aViewportWidth, int aViewportHeight, Color aViewportFillColor, Color aOddFieldColor, Color aEvenFieldColor, Side aMenuAlignment, Color aMenuFillColor, Color aMenuStrokeColor, double aMenuOpacity, int aBoardLoopTimeMillis ) {
		super( new BoulderDashPane( aCaveMap, aScreenWidth, aScreenHeight, aViewportWidth, aViewportHeight, aViewportFillColor, aOddFieldColor, aEvenFieldColor, aMenuAlignment, aMenuFillColor, aMenuStrokeColor, aMenuOpacity, aBoardLoopTimeMillis ) );
		_boulderDashPane = (BoulderDashPane) getRoot();
		Rockford theRockford = _boulderDashPane.getRockford();
		if ( theRockford != null ) {
			_boulderDashPane.centerViewortOffset( theRockford.getPositionX(), theRockford.getPositionY(), _boulderDashPane.getViewportWidth(), _boulderDashPane.getViewportHeight() );
			theRockford.onPositionChanged( evt -> _boulderDashPane.centerViewortOffset( theRockford.getPositionX(), theRockford.getPositionY(), _boulderDashPane.getViewportWidth(), _boulderDashPane.getViewportHeight() ) );
			setOnKeyPressed( evt -> {
				if ( evt.getCode() == KeyCode.UP ) {
					theRockford.moveUp();
				}
				else if ( evt.getCode() == KeyCode.DOWN ) {
					theRockford.moveDown();
				}
				else if ( evt.getCode() == KeyCode.LEFT ) {
					theRockford.moveLeft();
				}
				else if ( evt.getCode() == KeyCode.RIGHT ) {
					theRockford.moveRight();
				}
			} );
			setOnKeyReleased( evt -> {
				if ( theRockford.isMoveLeft() && evt.getCode() == KeyCode.LEFT ) {
					theRockford.halt();
				}
				else if ( theRockford.isMoveRight() && evt.getCode() == KeyCode.RIGHT ) {
					theRockford.halt();
				}
				else if ( theRockford.isMoveUp() && evt.getCode() == KeyCode.UP ) {
					theRockford.halt();
				}
				else if ( theRockford.isMoveDown() && evt.getCode() == KeyCode.DOWN ) {
					theRockford.halt();
				}
			} );
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// INJECTION:
	// /////////////////////////////////////////////////////////////////////////

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void start() {
		_boulderDashPane.start();
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////
}
